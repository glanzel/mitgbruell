import json
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime

@csrf_exempt
def save_malen(request, template='user/create.html'):
    d = {}
    if request.method == 'POST':
        print(request.body)
        dateTimeObj = datetime.now()
        timestampStr = dateTimeObj.strftime("%Y-%m-%d--%H-%M-%S.%f")
        filename = "malen/svg_"+timestampStr+".svg"; 
        f = open(filename, "x")
        body_unicode = request.body.decode('utf-8')
        f.write(body_unicode)
        f.close()
    response = JsonResponse({'saved': 'svg'})
    return response

