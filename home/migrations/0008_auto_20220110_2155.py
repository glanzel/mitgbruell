# Generated by Django 3.1.7 on 2022-01-10 21:55

from django.db import migrations
import wagtail.core.blocks
import wagtail.core.fields
import wagtail.embeds.blocks
import wagtail.images.blocks
import wagtailvideos.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0007_auto_20211118_1804'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contentpage',
            name='body',
            field=wagtail.core.fields.StreamField([('image', wagtail.core.blocks.StructBlock([('heading', wagtail.core.blocks.CharBlock(form_classname='full title')), ('image', wagtail.images.blocks.ImageChooserBlock(required=False)), ('width', wagtail.core.blocks.ChoiceBlock(choices=[(2, '2'), (3, '3'), (6, '6')], icon='cup'))], template='home_blocks/image_block.html')), ('w2_content_paragraph', wagtail.core.blocks.RichTextBlock(features=['h2', 'h3', 'h4', 'bold', 'italic', 'ol', 'ul', 'document-link', 'link'], template='cp_blocks/w2_content_paragraph.html')), ('video', wagtailvideos.blocks.VideoChooserBlock(template='cp_blocks/video.html')), ('embedVideo', wagtail.embeds.blocks.EmbedBlock(template='cp_blocks/w2_video.html')), ('image_slider', wagtail.core.blocks.StructBlock([('width', wagtail.core.blocks.ChoiceBlock(choices=[(2, '2'), (3, '3'), (6, '6')], icon='cup')), ('ratio', wagtail.core.blocks.ChoiceBlock(choices=[(10, '1:1'), (15, '2:3'), (6, '3:2')], icon='cup')), ('images', wagtail.core.blocks.ListBlock(wagtail.core.blocks.StructBlock([('heading', wagtail.core.blocks.CharBlock(form_classname='full title', required=False)), ('image', wagtail.images.blocks.ImageChooserBlock(required=False))])))], template='home_blocks/image_slider.html')), ('rawhtml', wagtail.core.blocks.RawHTMLBlock())]),
        ),
        migrations.AlterField(
            model_name='projectpage',
            name='body',
            field=wagtail.core.fields.StreamField([('image', wagtail.core.blocks.StructBlock([('heading', wagtail.core.blocks.CharBlock(form_classname='full title')), ('image', wagtail.images.blocks.ImageChooserBlock(required=False)), ('width', wagtail.core.blocks.ChoiceBlock(choices=[(2, '2'), (3, '3'), (6, '6')], icon='cup'))], template='home_blocks/image_block.html')), ('w2_content_paragraph', wagtail.core.blocks.RichTextBlock(features=['h2', 'h3', 'h4', 'bold', 'italic', 'ol', 'ul', 'document-link', 'link'], template='cp_blocks/w2_content_paragraph.html')), ('video', wagtailvideos.blocks.VideoChooserBlock(template='cp_blocks/video.html')), ('embedVideo', wagtail.embeds.blocks.EmbedBlock(template='cp_blocks/w2_video.html')), ('image_slider', wagtail.core.blocks.StructBlock([('width', wagtail.core.blocks.ChoiceBlock(choices=[(2, '2'), (3, '3'), (6, '6')], icon='cup')), ('ratio', wagtail.core.blocks.ChoiceBlock(choices=[(10, '1:1'), (15, '2:3'), (6, '3:2')], icon='cup')), ('images', wagtail.core.blocks.ListBlock(wagtail.core.blocks.StructBlock([('heading', wagtail.core.blocks.CharBlock(form_classname='full title', required=False)), ('image', wagtail.images.blocks.ImageChooserBlock(required=False))])))], template='home_blocks/image_slider.html')), ('rawhtml', wagtail.core.blocks.RawHTMLBlock())]),
        ),
    ]
