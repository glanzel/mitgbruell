import datetime
#from builtins import getattr

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.safestring import mark_safe
from django.template.loader import render_to_string
#from django.db.models.fields import CharField
from django.conf import settings
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
#from django.utils.html import format_html, format_html_join
#from django.forms.utils import flatatt
#from django.forms.fields import ChoiceField

from wagtail.core import blocks
from wagtail.core.models import Page
from wagtail.core.fields import RichTextField, StreamField
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel, MultiFieldPanel, FieldRowPanel, InlinePanel
from wagtail.images.blocks import ImageChooserBlock
#from wagtail.documents.blocks import DocumentChooserBlock
#from wagtail.embeds.blocks import EmbedBlock
from wagtail.images import get_image_model_string
from wagtail.images.edit_handlers import ImageChooserPanel

#from wagtailmedia.blocks import AbstractMediaChooserBlock
from wagtail.search import index
from wagtail_color_panel.fields import ColorField
from wagtail_color_panel.edit_handlers import NativeColorPanel
from wagtail.embeds.blocks import EmbedBlock
from wagtailvideos.edit_handlers import VideoChooserPanel
from wagtailvideos.blocks import VideoChooserBlock
#from wagtail_blocks.blocks import ImageSliderBlock
from wagtail.snippets.models import register_snippet
from wagtail.snippets.edit_handlers import SnippetChooserPanel

from taggit.models import TaggedItemBase
from modelcluster.fields import ParentalKey
from modelcluster.contrib.taggit import ClusterTaggableManager
from wagtail.core.blocks.list_block import ListBlock


@register_snippet
class IntroSnippet(models.Model):
    body = RichTextField(blank=True)
    panels = [
        FieldPanel('body'),
    ]
    def __str__(self):
        return self.body

class PageTag(TaggedItemBase):
    content_object = ParentalKey(
        'ProjectPage',
        related_name='tagged_items',
        on_delete=models.CASCADE,
    )


class SimpleImageBlock(blocks.StructBlock):
    heading = blocks.CharBlock(form_classname="full title", required=False)
    image = ImageChooserBlock(required=False)

    class Meta:
        template = "home_blocks/simple_image_block.html"
        #label = "ContentBlock"

class ImageBlock(blocks.StructBlock):
    image = ImageChooserBlock(required=False)
    heading = blocks.RichTextBlock(label="bildunterschrift", required=False)

    #link = blocks.PageChooserBlock(label="TheLink",required=False, null=True)

    width = blocks.ChoiceBlock(choices=[(2, '2'), (3, '3'), (6, '6')], icon='cup')

    class Meta:
        template = "home_blocks/multi_content_block.html"
        #label = "ContentBlock"

    def get_context(self, value, parent_context=None):
        context = super().get_context(value, parent_context=parent_context)
        return context

class ImageSlider(blocks.StructBlock):
    width = blocks.ChoiceBlock(choices=[(2, '2'), (3, '3'), (6, '6')], icon='cup')
    ratio = blocks.ChoiceBlock(choices=[ (10, '1:1'), (15, '2:3'), (6, '3:2')], icon='cup')
    images = ListBlock(SimpleImageBlock())
    def get_context(self, value, parent_context=None):
        context = super().get_context(value, parent_context=parent_context)
        print(self.__dict__)
        context["ratio"] =  int(value['ratio'])
        context["height"] = int(value['width']) * int(value['ratio'])
        return context


class SliderBlock(blocks.ListBlock):
    width = blocks.ChoiceBlock(choices=[(2, '2'), (3, '3'), (6, '6')], icon='cup')
    


class AppPage(Page):
    
    color = ColorField(default="#FFFFFF")
    
    date = models.DateField(
        "Post date", default=datetime.datetime.today,
        help_text=_("For Future use as Event only.")
    )
    teaser = RichTextField(blank=True)   

    class Meta:
        abstract = True
    
    settings_panels = [NativeColorPanel('color')] + Page.settings_panels + [ FieldPanel('date')]
    
    def getSubmenu(self):
        return self.get_children().filter(live=True, show_in_menus=True)


class AbstractContentPage(AppPage):
    class Meta:
        abstract = True

    tags = ClusterTaggableManager(through=PageTag, blank=True)
    
    image = models.ForeignKey(
        get_image_model_string(),
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name='+',
        verbose_name=_('Header image')
    )
    
    header_video = models.ForeignKey('wagtailvideos.Video',
                                     related_name='+',
                                    blank=True,
                                     null=True,
                                     on_delete=models.SET_NULL)
    
    body = StreamField([('image', ImageBlock(template="home_blocks/image_block.html")),
                        ('content_paragraph', blocks.RichTextBlock(template="cp_blocks/w2_content_paragraph.html",features=['h2', 'h3', 'h4', 'bold', 'italic', 'ol', 'ul', 'document-link', 'link'])),
                        ('video', VideoChooserBlock(template="cp_blocks/video.html")),
                        ("embedVideo",  EmbedBlock(template="cp_blocks/w2_video.html")),
                        
                        ("image_slider", ImageSlider(template="home_blocks/image_slider.html")),
                        #('image_slider', ImageSliderBlock()),
                        ("spacer", blocks.StaticBlock(template="blocks/spacer.html")),
                        ("rawhtml", blocks.RawHTMLBlock()),
                        ])

    content_panels = AppPage.content_panels + [
        ImageChooserPanel('image'),
        VideoChooserPanel('header_video'),
        FieldPanel('teaser'),
        StreamFieldPanel('body', classname="full"),
    ]
    
    search_fields = Page.search_fields + [
        index.SearchField('body'),
    ]

class ContentPage(AbstractContentPage):
    pass

class ProjectPage(AbstractContentPage):
    main_text = RichTextField(blank=True, verbose_name="Summary (auf Detailseite)")
    #tags = models.CharField(max_length=255)
    
    content_panels = AppPage.content_panels + [
        FieldPanel('tags'),
        ImageChooserPanel('image'),
        VideoChooserPanel('header_video'),
        FieldPanel('teaser'),
        FieldPanel('main_text'),
        StreamFieldPanel('body', classname="full"),
    ]
    
    def prev_project(self):
        if self.get_prev_siblings().live().first():
            print(self.get_prev_siblings().live().first().url)
            return self.get_prev_siblings().live().first().url
        else:
            print("get last sibling")
            return self.get_siblings().live().last().url

    def next_project(self):
        if self.get_next_siblings().live().first():
            return self.get_next_siblings().live().first().url
        else:
            return self.get_siblings().live().first().url
    

#subblock for other strut blocks
class CssRulesBlock(blocks.StructBlock):
    width = blocks.ChoiceBlock(choices=[(1, '1'), (2, '2'), (3, '3')], classname="short_dropdown")
    height = blocks.ChoiceBlock(choices=[(1, '1'), (2, '2')],  form_classname="short_dropdown")
    background = blocks.ChoiceBlock(choices=[(2, "image"), (1, "color"), (0, "no")],  form_classname="short_dropdown")
    class Meta:
        form_classname="css_rules_block"

#subblock for latest Childs strut blocks
class RulesBlock(blocks.StructBlock):
    width = blocks.ChoiceBlock(choices=[(1, '1'), (2, '2'), (3, '3')], classname="short_dropdown")
    number = blocks.ChoiceBlock(choices=[(0, '0') ,(1, '1'), (2, '2')], icon='cup')
    background = blocks.ChoiceBlock(choices=[(2, "image"), (1, "color"), (0, "no")],  form_classname="short_dropdown")
    class Meta:
        form_classname="css_rules_block"

class UserdrawBlock(blocks.StructBlock):
    starttext = blocks.CharBlock(required=False)
    image = ImageChooserBlock(required=False)
    class Meta:
        template = "home_blocks/malen.html"
        form_classname="short_dropdowns"

class LatestChildsBlock(blocks.StructBlock):
    sum_page = blocks.PageChooserBlock()
    number = blocks.CharBlock()
    class Meta:
        template = "home_blocks/latest_childs_block.html"

    def get_context(self, value, parent_context=None):
        context = super().get_context(value, parent_context=parent_context)
        print(value["number"])
        limit_no = int(value["number"])
        live_childs = value['sum_page'].specific.get_children().live()[:limit_no]
        
        try: context["items"] = live_childs #TODO: muss später noch begrenzt werden
        except: context["items"] = "empty"
        print(context["items"]) 
        return context

class SummarizePageBlock(blocks.StructBlock):
    sum_page = blocks.PageChooserBlock(classname="long_dropdown", target_model=ContentPage)
    block_template = blocks.ChoiceBlock(choices=[("home_blocks/page_summarize_block.html", 'Projects'), ("home_blocks/teampage_summarize.html", 'Team')], icon='cup')

    #css_rules = CssRulesBlock(label="Rules")
    class Meta:
        form_classname="short_dropdowns"
    
    def get_context(self, value, parent_context=None):
        context = super().get_context(value, parent_context=parent_context)
        return context
    
    def render(self, value, context=None):
        template = "home_blocks/page_summarize_block.html";
        if value['block_template']: template = value['block_template']
 
        if context is None: new_context = self.get_context(value)
        else: new_context = self.get_context(value, parent_context=dict(context))

        return mark_safe(render_to_string(template, new_context))


class HomePage(AppPage):
    
    body = StreamField([('image', ImageBlock(template="home_blocks/image_block.html")),
                        ('RichText', blocks.RichTextBlock(template="cp_blocks/w2_content_paragraph.html",features=['h2', 'h3', 'h4', 'bold', 'italic', 'ol', 'ul', 'document-link', 'link'])),
                        ('userdraw', UserdrawBlock()),
                        ('latestChilds', LatestChildsBlock()),
                        ('pageSummarize', SummarizePageBlock()),
                        ("spacer", blocks.StaticBlock(template="blocks/spacer.html")),
                        ("rawhtml", blocks.RawHTMLBlock()),
                        ], blank=True, null=True)
        
    @property
    def template(self):
        return "home/home_page.html"

    content_panels = AppPage.content_panels + [
        StreamFieldPanel('body', classname="full"),
    ]
    
    subpage_types = ['HomePage', 'ContentPage', 'IndexPage','ProjectIndexPage', 'ParentPage']

class ParentPage(HomePage):
    image = models.ForeignKey(
        get_image_model_string(),
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name='+',
        verbose_name=_('Header image')
    )
     
    @property
    def template(self):
        return "home/parent_page.html"

    content_panels = AppPage.content_panels + [
        ImageChooserPanel('image'),
        StreamFieldPanel('body', classname="full"),
    ]


class AbstractIndexPage(AppPage):
    class Meta:
        verbose_name = _('Index')
        abstract = True

    @property
    def childs(self):
        # Get list of child pages that are descendants of this page
        mychilds = self.get_children().specific().live()
        #mychilds = ProjectPage.objects.live().public()
                
        # print("IndexPage getChilds:")
        # print(childs)
        return mychilds

    def get_context(self, request, *args, **kwargs):
        context = super(AbstractIndexPage, self).get_context(request, *args, **kwargs)
        mychilds = self.childs
        
        if request.GET.get('tag', None):
            tags = request.GET.get('tag')
            mychilds = mychilds.filter(tags__slug__in=[tags])
        
        # Pagination
        page = request.GET.get('page')
        page_size = 16
        if hasattr(settings, 'BLOG_PAGINATION_PER_PAGE'):
            page_size = settings.BLOG_PAGINATION_PER_PAGE

        paginator = None
        if page_size is not None:
            paginator = Paginator(mychilds, page_size)  # Show 10 blogs per page
            try:
                childs = paginator.page(page)
            except PageNotAnInteger:
                childs = paginator.page(1)
            except EmptyPage:
                childs = paginator.page(paginator.num_pages)

        context['childs'] = mychilds
        context['paginator'] = paginator
        context['display'] = {}
        context['display']['rules'] = {"width":"4", "background":"2"}
        #context = get_blog_context(context)
        return context
        
class ProjectIndexPage(AbstractIndexPage):
    class Meta:
        verbose_name = _('Project Index Page')

    @property
    def childs(self):
        # Ich neme hier einfach alle Project Pages. benötige diese damit tags funktionieren.
        mychilds = ProjectPage.objects.live().public()
        return mychilds
 
 
    def get_template(self, request):
        return 'home/index_page.html'
 
    subpage_types = ['ProjectPage']



class IndexPage(AbstractIndexPage):
    class Meta:
        verbose_name = _('Index Page')

    def get_template(self, request):
        return 'home/index_page.html'

    subpage_types = ['ProjectPage', 'ContentPage']
    
