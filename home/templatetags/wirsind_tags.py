from django import template
from home.models import IntroSnippet
register = template.Library()

# Intro snippets
#@register.simple_tag(takes_context=True)
@register.inclusion_tag('tags/intro.html',takes_context=True)
def intro(context):
    return {
        'intro': IntroSnippet.objects.all().last(),
        'request': context['request']
    }
